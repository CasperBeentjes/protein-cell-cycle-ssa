import numpy as np
from scipy.stats import *

## Create a manual distribution to give the deterministic cell-cycle ##
class delta_dist(rv_continuous):
    def _pdf(self,x):
        if x == 0.0:
            return np.inf
        else:
            return 0.0
    def _ppf(self,x):
        return 0.0

def SSA(n0=0, t0=0, phase0=0, age0=0, Tmax=10., Tcycle=1., phase_cycle=0.5, Ncycle=np.inf, 
        burst_rate=1., burst_size=1., decay_rate=0):
    """
    SSA to simulate population observation for Models II, III and IV in the paper:
    "Exact solution of stochastic gene expression models with bursting, cell cycle 
    and replication dynamics" by Beentjes, Perez-Carrasco & Grima

    Input: 
        n0          - initial protein level of cell (integer, can be array/list)
        phase0      - initial phase of cell (0 or 1, can be array/list)
        age0        - initial age of cell within phase (float+, can be array/list)
        t0          - initial time at which we start to measure the cell (float+, can be array/list)
        Tmax        - final time of observation (float+)
        Tcycle      - length of the (average) cell cycle (float+)
        phase_cycle - point in (average) cell cycle at which we enter post replication stage (0<float<1)
        Ncycle      - cell-cycle variability parameter, number of stages in Erlang model (float+)
        burst_rate  - rate at which to create protein bursts (float+)
        burst_size  - average size per protein burst that gets created (geometric distribution) (float+)
        decay_rate  - rate at which protein molecules decay (float+)
    Output:
        P           - array containing:
                        first row the measurement times
                        second row the cell content
                        third row the cell age within current phase at measurement
                        fourth row the cell phase at measurement

    """
    # Check if Ncycle takes realistic value, i.e. N>0
    assert Ncycle > 0, "Need Ncycle > 0."
    # Check if change of phase in cycle takes realistic value, i.e. 0<=u<=1
    assert (0 <= phase_cycle <= 1), "Need phase_cycle in [0,1]."
    # Value for u=M/N
    u = phase_cycle
    
    # Check whether the initial conditions provided are of consistent size, i.e.
    # need n0, t0, age0, phase0 to be either the same size, or being a scalar.
    n0 = np.asarray(n0).flatten(); t0 = np.asarray(t0).flatten()  
    phase0 = np.asarray(phase0).flatten(); age0 = np.asarray(age0).flatten()
    # Check if initial values have consistent sizes
    if np.size(n0) != np.size(t0):
        if np.size(n0) == 1:
            n0 = n0*np.ones(np.size(t0))
        elif np.size(t0) == 1:
            t0 = t0*np.ones(np.size(n0))
        else:
            raise ValueError('Initial values are not consistent.')    
    if np.size(phase0) != np.size(age0):
        if np.size(phase0) == 1:
            if np.size(n0) == 1:
                phase0 = phase0*np.ones(np.size(age0))
                n0 = n0*np.ones(np.size(age0))
                t0 = t0*np.ones(np.size(age0))                              
            elif np.size(n0) != np.size(age0):
                raise ValueError('Initial values are not consistent.')
            else:
                phase0 = phase0*np.ones(np.size(age0))
        elif np.size(age0) == 1:
            if np.size(n0) == 1:
                age0 = age0*np.ones(np.size(phase0))
                n0 = n0*np.ones(np.size(phase0))
                t0 = t0*np.ones(np.size(phase0))
            elif np.size(n0) != np.size(phase0):
                raise ValueError('Initial values are not consistent.') 
            else:
                age0 = age0*np.ones(np.size(phase0))
        else:
            raise ValueError('Initial values are not consistent.')
    else:
        if np.size(phase0) == 1:
            phase0 = phase0*np.ones(np.size(n0))
            age0 = age0*np.ones(np.size(n0))
        elif np.size(phase0) != np.size(n0):
            raise ValueError('Initial values are not consistent.')         
        
    # Burst rate in pre replication and post replication phase
    b1 = burst_rate
    b2 = 2*burst_rate
    
    # Pick the cell cycle length distribution 
    # If Ncycle infinity we have deterministic cell cycles
    if Ncycle ==  np.inf:
        rv1 = delta_dist().freeze(loc=u*Tcycle)
        rv2 = delta_dist().freeze(loc=(1-u)*Tcycle)
    # Otherwise we use an Erlang distribution, note that
    # it is allowed to take "a" non-integer, the Erlang distribution
    # gets converted into a Gamma distribution by scipy automatically
    else:
        rv1 = erlang(a=u*Ncycle, scale=Tcycle/Ncycle)
        rv2 = erlang(a=(1-u)*Ncycle, scale=Tcycle/Ncycle)    
        
    # Define a queue Q which holds a list of times (first row), protein counts (second row) 
    # age within phase (third row) and cell phase (fourth row) 
    # from which we need to simulate until time t.

    # Estimate from population growth how big Q should be if we start from a single cell
    if Ncycle == np.inf:
        qx_max = 2**int(Tmax+0.001)+1
    else:
        qx_max = int((np.exp(Ncycle*(2**(1./Ncycle)-1)))**(Tmax))
    # Account for possibility of starting from multiple cells
    qx_max *= np.size(n0)   
    Q = np.zeros((4,qx_max))
    
    # Pre-allocate array to save the protein levels at final time
    P = np.zeros((4,qx_max))
    px = 0
    
    # Read initial values supplied and write to the queue
    for qx in range(np.size(n0)):
        # Initial protein level
        protein = n0[qx]
        # Initial absolute time for the cell (relative to Tmax measurement time)
        t = t0[qx]
        # Initial phase of the cell, where
        # phase 0 = pre replication
        # phase 1 = post replication
        phase = phase0[qx]
        # Initial age of the cell within the phase
        age = age0[qx]
        Q[:,qx]= np.array([t,protein,age,phase])
    qx += 1
    
    # Iterate until we have finished all the cells in the queue
    while px < qx:

        # Load the next element from the queue to simulate from
        t = Q[0,px]
        protein = Q[1,px]
        age = Q[2,px]
        phase = Q[3,px]
        
        # Print some progression stats, roughly at every 10% of number of cells we expect to simulate
        if px % int(qx_max/10) == 0:
            print("Completed %d cells, queue length %d, expected final queue length %d" %(px,qx,qx_max))

        # Time until the next cell phase/division event
        # Need to account for possiblity of age > 0, i.e. we need to generate from the truncated
        # distribution given that the next cell progression event > current age
        if phase == 0:
            if Ncycle == np.inf:
                assert age < u*Tcycle, "Need initial age consistent with phase and interdivision time"
                tnext = rv1.ppf(np.random.rand()) - age
            else:
                tnext = rv1.ppf((1-rv1.cdf(age))*np.random.rand() + rv1.cdf(age))
            burst_rate = b1
        else:
            if Ncycle == np.inf:
                assert age < (1-u)*Tcycle, "Need initial age consistent with phase and interdivision time"
                tnext = rv2.ppf(np.random.rand()) - age
            else:
                tnext = rv2.ppf((1-rv2.cdf(age))*np.random.rand() + rv2.cdf(age))
            burst_rate = b2

        # Account for degenerate (one-phase) case
        if np.isnan(tnext):
            tnext = 0                 

        # Geometric distribution properties from burst_size
        p_geom = 1./(burst_size+1)
        lp = np.log(1-p_geom)
        ## Pre-calculate random variables to speed up calculations
        # Make explicit guess as to how many geometric variables we need to generate
        K = int(3*(Tmax-t)*burst_rate)
        kx = 0
        # Pre-generate K bursts of protein to re-use in the simulation
        G = np.ceil(np.log(np.random.rand(K))/lp - 1)
        # Make explicit guess as to how many uniform random variables we need to generate
        L = int(3*(Tmax-t)*burst_rate)
        lx = 0
        # Pre-generate L unit-rate waiting times
        logU = -np.log(np.random.rand(L))
        
        while t < Tmax:
            # Calculate the total reaction event propensity
            prop_total = burst_rate + decay_rate*protein
            # Calculate the propensity to see a protein burst, rather than decay
            burst_prob = burst_rate/prop_total
            # Generate the waiting time
            # Check if we can use pre-generated random variables
            if lx < L:
                dt = logU[lx]/prop_total
                lx += 1
            # Otherwise generate explicit one-by-one
            else:
                dt = - np.log(np.random.rand())/prop_total
            # Check whether first to fire reaction or change cell phase
            # 1) Biochemical reactions
            if tnext > dt:
                # Check if we don't go over final time
                if t + dt < Tmax:
                    # Create burst of protein
                    if np.random.rand() < burst_prob:
                        # Check if we can use pre-generated random variables
                        if kx < K:
                            protein += G[kx]
                            kx += 1
                        # Otherwise generate explicit one-by-one
                        else:
                            protein += np.ceil(np.log(np.random.rand())/lp - 1)
                    # Degrade protein
                    else:
                        protein -= 1
                    # Update the time to next cell cycle event
                    tnext -= dt
                    # Update the simulation time
                    t += dt 
                    # Update cell age
                    age += dt
                else:
                    # Update the cell age
                    age += (Tmax-t)
                    t += (Tmax-t)
                    # Final time reached without further reaction, save state
                    P[0,px] = t
                    P[1,px] = protein
                    P[2,px] = age
                    P[3,px] = phase
                    px += 1
                    # Final time reached, break to end simulation of this cell
                    break
            # 2) Phase progression
            else:
                dt = tnext
                if t + dt < Tmax:               
                    # Update the simulation time
                    t += dt   
                    # Update cell age                        
                    age += dt
                    # Update the phase and time to next phase change
                    if phase == 0:
                        # Update cell age                                
                        age = 0                                
                        # Progress to second phase
                        phase = 1
                        # Set the correct burst rate
                        burst_rate = b2
                        # Generate time till next phase change
                        tnext = rv2.ppf(np.random.rand())
                    else:
                        # Apply the binomial partitioning
                        if protein > 0:
                            protein_other = np.random.binomial(protein, 0.5)
                            # Update the queue with new cell at time t with contents protein_other 
                            # and age=0 and phase=0
                            Q[:,qx] = np.array([t,protein_other,0,0])
                            qx += 1
                            # Check if queue still long enough, otherwise extend by doubling size
                            if qx >= qx_max:
                                Q = np.concatenate((Q,np.zeros(np.shape(Q))),axis=1)   
                                P = np.concatenate((P,np.zeros(np.shape(P))),axis=1)
                                qx_max *= 2
                            # Update the current cell contents, 
                            # remove the binomial partitioning part which is in the queue
                            protein -= protein_other
                        # Progress to pre replication phase
                        # Update cell age                                
                        age = 0                                
                        # Progress to first phase
                        phase = 0            
                        # Set the correct burst rate
                        burst_rate = b1  
                        # Generate time till next phase change
                        tnext = rv1.ppf(np.random.rand()) 
                    # Account for degenerate (one-phase) case
                    if np.isnan(tnext):
                        tnext = 0
                else:
                    # Update the cell age
                    age += (Tmax-t)
                    t += (Tmax-t)
                    # Final time reached without further reaction, save state
                    P[0,px] = t
                    P[1,px] = protein
                    P[2,px] = age
                    P[3,px] = phase
                    px += 1
                    # Final time reached, break to end simulation of this cell
                    break
                    
    print("Completed %d cells, final queue length %d" %(px,qx))
    # Return the actual observations, i.e. up until last written index
    return P[:,:px]