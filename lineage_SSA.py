import numpy as np
from scipy.stats import *

## Create a manual distribution to give the deterministic cell-cycle ##
class delta_dist(rv_continuous):
    def _pdf(self,x):
        if x == 0.0:
            return np.inf
        else:
            return 0.0
    def _ppf(self,x):
        return 0.0

def SSA(n0=0, t0=0, phase0=0, age0=0, Tmax=10., Tcycle=1., phase_cycle=0.5, Ncycle=np.inf, 
        measurement_rate=1., burst_rate=1., burst_size=1., decay_rate=0):
    """
    SSA to simulate lineage observation for Models II, III and IV in the paper:
    "Exact solution of stochastic gene expression models with bursting, cell cycle 
    and replication dynamics" by Beentjes, Perez-Carrasco & Grima

    Input: 
        n0          - initial protein level of cell (integer)
        phase0      - initial phase of cell (0 or 1)
        age0        - initial age of cell within phase (float+)
        t0          - initial time at which we start to measure the cell (float+)
        Tmax        - final time of observation (float+)
        Tcycle      - length of the (average) cell cycle (float+)
        phase_cycle - point in (average) cell cycle at which we enter post replication stage (0<float<1)
        Ncycle      - cell-cycle variability parameter, number of stages in Erlang model (float+)
        measurement_rate - take measurements every 1/measurement_rate units of time (float+)
        burst_rate  - rate at which to create protein bursts (float+)
        burst_size  - average size per protein burst that gets created (geometric distribution) (float+)
        decay_rate  - rate at which protein molecules decay (float+)
    Output:
        P           - array containing:
                        first row the measurement times
                        second row the cell content
                        third row the cell age within current phase at measurement
                        fourth row the cell phase at measurement

    """
    # Check if Ncycle takes realistic value, i.e. N>0
    assert Ncycle > 0, "Need Ncycle > 0."
    # Check if change of phase in cycle takes realistic value, i.e. 0<=u<=1
    assert (0 <= phase_cycle <= 1), "Need phase_cycle in [0,1]."
    # Value for u=M/N
    u = phase_cycle

    # Initial protein level
    protein = n0
    # Initial time, start of the first cell cycle
    t = 0
    # Initial phase of the cell, where
    # phase 0 = pre replication
    # phase 1 = post replication
    phase = phase0
    # Initial age of the cell within the phase
    age = age0

    # Pick the cell cycle length distribution 
    # If Ncycle infinity we have deterministic cell cycles
    if Ncycle ==  np.inf:
        rv1 = delta_dist().freeze(loc=u*Tcycle)
        rv2 = delta_dist().freeze(loc=(1-u)*Tcycle)
    # Otherwise we use an Erlang distribution, note that
    # it is allowed to take "a" non-integer, the Erlang distribution
    # gets converted into a Gamma distribution by scipy automatically
    else:
        rv1 = erlang(a=u*Ncycle, scale=Tcycle/Ncycle)
        rv2 = erlang(a=(1-u)*Ncycle, scale=Tcycle/Ncycle)

    # Burst rate in pre replication and post replication phase
    b1 = burst_rate
    b2 = 2*burst_rate

    # Time until the next cell phase/division event
    # Need to account for possiblity of age > 0, i.e. we need to generate from the truncated
    # distribution given that the next cell progression event > current age
    if phase == 0:
        if Ncycle == np.inf:
            assert age < u*Tcycle, "Need initial age consistent with phase and interdivision time"
            tnext = rv1.ppf(np.random.rand()) - age
        else:
            tnext = rv1.ppf((1-rv1.cdf(age))*np.random.rand() + rv1.cdf(age))
        burst_rate = b1
    else:
        if Ncycle == np.inf:
            assert age < (1-u)*Tcycle, "Need initial age consistent with phase and interdivision time"
            tnext = rv2.ppf(np.random.rand()) - age
        else:
            tnext = rv2.ppf((1-rv2.cdf(age))*np.random.rand() + rv2.cdf(age))
        burst_rate = b2

    # Account for degenerate (one-phase) case
    if np.isnan(tnext):
        tnext = 0                 

    # Pre-allocate array to save the protein levels at regular intervals
    P = np.zeros((4,int((Tmax-t0)*measurement_rate)+1))
    P.fill(np.nan)
    # Time until next measurement
    dTm = 1./measurement_rate
    # Incorporate the possibility of unknown start of measurement
    Tm = t0 
    # Index of current entry in memory array
    px = 0
    
    # Geometric distribution properties from burst_size
    p_geom = 1./(burst_size+1)
    lp = np.log(1-p_geom)
    ## Pre-calculate random variables to speed up calculations
    # Make explicit guess as to how many geometric variables we need to generate
    K = int(3*(Tmax-t)*burst_rate)
    kx = 0
    # Pre-generate K bursts of protein to re-use in the simulation
    G = np.ceil(np.log(np.random.rand(K))/lp - 1)
    # Make explicit guess as to how many uniform random variables we need to generate
    L = int(3*(Tmax-t)*burst_rate)
    lx = 0
    # Pre-generate L unit-rate waiting times
    logU = -np.log(np.random.rand(L))
    
    while t < Tmax:
        # Calculate the total reaction event propensity
        prop_total = burst_rate + decay_rate*protein
        # Calculate the propensity to see a protein burst, rather than decay
        burst_prob = burst_rate/prop_total
        # Generate the waiting time
        # Check if we can use pre-generated random variables
        if lx < L:
            dt = logU[lx]/prop_total
            lx += 1
        # Otherwise generate explicit one-by-one
        else:
            dt = - np.log(np.random.rand())/prop_total
        # Check whether first to fire reaction or change cell phase
        # 1) Biochemical reactions
        if tnext > dt:
            # Check if we don't go over final time
            if t + dt < Tmax:
                # Check if we need to save measurements first
                while t + dt > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    # Update the next measurement time
                    Tm += dTm
                # Create burst of protein
                if np.random.rand() < burst_prob:
                    # Check if we can use pre-generated random variables
                    if kx < K:
                        protein += G[kx]
                        kx += 1
                    # Otherwise generate explicit one-by-one
                    else:
                        protein += np.ceil(np.log(np.random.rand())/lp - 1)
                # Degrade protein
                else:
                    protein -= 1
                # Update the time to phase change
                tnext -= dt
                # Update the simulation time
                t += dt 
                # Update the cell age
                age += dt
            else:
                # We reach final time before next reaction occurs
                # Check if we need to save measurements first
                while Tmax > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    Tm += dTm
                # Final time reached, break to end simulation
                break
        # 2) Phase progression
        else:
            dt = tnext
            if t + dt < Tmax:               
                # Check if we need to save measurements
                while t + dt > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    Tm += dTm
                # Update the simulation time
                t += dt    
                # Update the phase and time to next phase change
                if phase == 0:
                    # Progress to post replication phase
                    phase = 1
                    age = 0
                    tnext = rv2.ppf(np.random.rand())
                    burst_rate = b2
                else:
                    # Apply the binomial partitioning
                    if protein > 0:
                        protein = np.random.binomial(protein, 0.5)
                    # Progress to pre replication phase
                    # Update cell age                                
                    age = 0                                
                    # Progress to first phase
                    phase = 0            
                    # Set the correct burst rate
                    burst_rate = b1  
                    # Generate time till next phase change
                    tnext = rv1.ppf(np.random.rand()) 
                # Account for degenerate (one-phase) case when u=0 or 1
                if np.isnan(tnext):
                    tnext = 0
            else:
                # We reach final time before next reaction occurs
                # Check if we need to save measurements first
                while Tmax > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    Tm += dTm
                # Final time reached, break to end simulation
                break

    # Return the actual observations, i.e. up until last written index
    return P[:,:px]