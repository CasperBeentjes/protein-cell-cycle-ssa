import numpy as np

def SSA(n0=0, t0=0, Tmax=10., Tcycle=1., measurement_rate=1., 
        burst_rate=1., burst_size=1., decay_rate=0):
    """
    SSA to simulate lineage observation for Model I in the paper:
    "Exact solution of stochastic gene expression models with bursting, cell cycle 
    and replication dynamics" by Beentjes, Perez-Carrasco & Grima

    Input: 
        n0          - initial protein level of cell (integer)
        t0          - initial time at which we start to measure the cell (float+)
        Tmax        - final time of observation (float+)
        Tcycle      - length of the (average) cell cycle (float+)
        measurement_rate - take measurements every 1/measurement_rate units of time (float+)
        burst_rate  - rate at which to create protein bursts (float+)
        burst_size  - average size per protein burst that gets created (geometric distribution) (float+)
        decay_rate  - rate at which protein molecules decay (float+)
    Output:
        P           - array containing:
                        first row the measurement times
                        second row the cell content
    """
    # Effective protein decay_rate for a lineage
    decay_rate = decay_rate + (2./3.)/Tcycle
    # Initial protein level
    protein = n0
    # Initial time, start of the first cell cycle
    t = 0
    
    # Pre-allocate array to save the protein levels at regular intervals
    P = np.zeros((2,int((Tmax-t0)*measurement_rate)+1))
    P.fill(np.nan)
    # Time until next measurement
    dTm = 1./measurement_rate
    # Incorporate the possibility of unknown start of measurement
    Tm = t0 
    # Index of current entry in memory array
    px = 0
    
    # Geometric distribution properties from burst_size
    p_geom = 1./(burst_size+1)
    lp = np.log(1-p_geom)
    ## Pre-calculate random variables to speed up calculations
    # Make explicit guess as to how many geometric variables we need to generate
    K = int(3*(Tmax-t0)*burst_rate)
    k = 0
    # Pre-generate K bursts of protein to re-use in the simulation
    G = np.ceil(np.log(np.random.rand(K))/lp - 1)
    # Make explicit guess as to how many uniform random variables we need to generate
    L = int(3*(Tmax-t0)*burst_rate)
    l = 0
    # Pre-generate L unit-rate waiting times
    logU = -np.log(np.random.rand(L))
    
    # Consider unstable protein case first
    while t < Tmax:
        # Calculate the total reaction event propensity
        prop_total = burst_rate + decay_rate*protein
        # Calculate the propensity to see a protein burst, rather than decay
        burst_prob = burst_rate/prop_total
        # Generate the waiting time
        if l < L:
            dt = logU[l]/prop_total
            l += 1
        else:
            dt = - np.log(np.random.rand())/prop_total
        # Check if we don't go over final time
        if t + dt < Tmax:
            # Check if we need to save measurements first
            while t + dt > Tm:
                P[0,px] = Tm
                P[1,px] = protein
                px += 1
                # Update the next measurement time
                Tm += dTm
            # Create burst of protein
            if np.random.rand() < burst_prob:
                # Check if we can use pre-generated random variables
                if k < K:
                    protein += G[k]
                    k += 1
                # Otherwise generate explicit one-by-one
                else:
                    protein += np.ceil(np.log(np.random.rand())/lp - 1)
            # Degrade protein
            else:
                protein -= 1
            # Update the simulation time
            t += dt                    
        else:
            # We reach final time before next reaction occurs
            # Check if we need to save measurements first
            while Tmax > Tm:
                P[0,px] = Tm
                P[1,px] = protein
                px += 1
                Tm += dTm
            # Final time reached, break to end simulation
            break

    # Return the actual observations, i.e. up until last written index
    return P[:,:px]