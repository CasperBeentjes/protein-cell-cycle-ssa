## Synopsis

This repository contains example code used for stochastic simulations in the paper [*"Exact solution of stochastic gene expression models with bursting, cell cycle and replication dynamics"*](https://doi.org/10.1103/PhysRevE.101.032403) by Beentjes, Perez-Carrasco & Grima.

For mathematical details, see 

    @article{beentjes2020,
    author = {Beentjes, Casper H. L. and Perez-Carrasco, Ruben and Grima, Ramon},    
    title = {Exact solution of stochastic gene expression models with bursting, cell cycle and replication dynamics},    
    doi = {10.1103/PhysRevE.101.032403},
    publisher = {American Physical Society},    
    journal = {Physical Review E},
    month = {mar},
    number = {3},
    pages = {032403},
    volume = {101},
    year = {2020},
    }

## Running the code

Repository contains a Jupyter notebook (ProteinModels.ipynb) which shows how to run the various SSAs.

Alternatively one can use the following SSA python implementations directly:

* lineage_SSA.py
    * implementation for lineage measurements for Models II, III, IV, note that the pre and post replication phase are Erlang distributed.
* population_SSA.py
    * implementation for population snapshot measurements for Models II, III, IV, note that the pre and post replication phase are Erlang distributed.
* ModelI_SSA.py
    * implementation for lineage equivalent for Model I
* ErlangModelLineage.py
    * implementation for lineage measurements for Models III and IV with explicit exponential phases

## Authors

C. Beentjes <beentjes@maths.ox.ac.uk>

R. Grima <ramon.grima@ed.ac.uk>

R. Perez-Carrasco <r.carrasco@ucl.ac.uk>