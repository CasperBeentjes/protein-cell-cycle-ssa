import numpy as np
from scipy.stats import *

## Create a manual distribution to give the deterministic cell-cycle ##
class delta_dist(rv_continuous):
    def _pdf(self,x):
        if x == 0.0:
            return np.inf
        else:
            return 0.0
    def _ppf(self,x):
        return 0.0

def SSA(n0=0, t0=0, phase0=0, age0=0, Tmax=10., Tcycle=1., phase_cycle=0.5, Ncycle=2, 
        measurement_rate=1., burst_rate=1., burst_size=1., decay_rate=0):
    """
    SSA to simulate lineage observation for Models II, III and IV in the paper:
    "Exact solution of stochastic gene expression models with bursting, cell cycle 
    and replication dynamics" by Beentjes, Perez-Carrasco & Grima

    NOTE: this implementation assumes that each phase length is i.i.d. distributed

    Input: 
        n0          - initial protein level of cell (integer)
        phase0      - initial phase of cell (0 or 1)
        age0        - initial age of cell within phase (float+)
        t0          - initial time at which we start to measure the cell (float+)
        Tmax        - final time of observation (float+)
        Tcycle      - length of the (average) cell cycle (float+)
        phase_cycle - point in (average) cell cycle at which we enter post replication stage (0<float<1)
        Ncycle      - cell-cycle variability parameter, number of stages in Erlang model (int+)
        measurement_rate - take measurements every 1/measurement_rate units of time (float+)
        burst_rate  - rate at which to create protein bursts (float+)
        burst_size  - average size per protein burst that gets created (geometric distribution) (float+)
        decay_rate  - rate at which protein molecules decay (float+)
    Output:
        P           - array containing:
                        first row the measurement times
                        second row the cell content
                        third row the cell age within current phase at measurement
                        fourth row the cell phase at measurement

    """
    assert isinstance(Ncycle, int), "Ncycle must be int"
    assert (Ncycle > 0), "Positive Ncycle needed"
    Mcycle = int(phase_cycle*Ncycle)  
    
    # Initial protein level
    assert isinstance(n0, int), "Initial protein must be int"
    protein = n0
    # Initial time, start of the first cell cycle
    t = 0
    # Initial phase of the cell, where
    # phase 0,..,Mcycle-1 = pre replication
    # phase Mcycle,...,Ncycle-1 = post replication
    assert (0<= phase0 <= Ncycle - 1), "Initial phase must be consistent with Ncycle"
    phase = int(phase0)
    # Initial age of the cell within the phase
    # NOTE: this model assumes exponential waiting times, so non-zero age has no
    # influence
    assert (age0 >= 0), "Initial age must be non-negative float"
    age = age0

    # Burst rate in pre replication and post replication phase
    b1 = burst_rate
    b2 = 2*burst_rate

    # Pre-allocate array to save the protein levels at regular intervals
    P = np.zeros((4,int((Tmax-t0)*measurement_rate)+1))
    P.fill(np.nan)
    # Time until next measurement
    dTm = 1./measurement_rate
    # Incorporate the possibility of unknown start of measurement
    Tm = t0 
    # Index of current entry in memory array
    px = 0

    # Geometric distribution properties from burst_size
    p_geom = 1./(burst_size+1)
    lp = np.log(1-p_geom)
    ## Pre-calculate random variables to speed up calculations
    # Make explicit guess as to how many geometric variables we need to generate
    K = int(3*(Tmax-t)*burst_rate)
    k = 0
    # Pre-generate K bursts of protein to re-use in the simulation
    G = np.ceil(np.log(np.random.rand(K))/lp - 1)
    # Make explicit guess as to how many uniform random variables we need to generate
    L = int(3*(Tmax-t)*burst_rate)
    l = 0
    # Pre-generate L unit-rate waiting times
    logU = -np.log(np.random.rand(L))

    # Time until the next cell phase/division event, note i.i.d. exponential r.v. 
    # Make explicit guess as to how many exponential variables we need to generate
    R = int(2*Tmax*Ncycle)
    rx = 0
    Dt = -np.log(np.random.rand(R))/Ncycle
    tnext = Dt[rx]
    rx += 1
    
    while t < Tmax:
        # Calculate the total reaction event propensity
        prop_total = burst_rate + decay_rate*protein
        # Calculate the propensity to see a protein burst, rather than decay
        burst_prob = burst_rate/prop_total
        # Generate the waiting time
        # Check if we can use pre-generated random variables
        if l < L:
            dt = logU[l]/prop_total
            l += 1
        # Otherwise generate explicit one-by-one
        else:
            dt = - np.log(np.random.rand())/prop_total
        # Check whether first to fire reaction or change cell phase
        # 1) Biochemical reactions
        if tnext > dt:
            # Check if we don't go over final time
            if t + dt < Tmax:
                # Check if we need to save measurements first
                while t + dt > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    # Update the next measurement time
                    Tm += dTm
                # Create burst of protein
                if np.random.rand() < burst_prob:
                    # Check if we can use pre-generated random variables
                    if k < K:
                        protein += G[k]
                        k += 1
                    # Otherwise generate explicit one-by-one
                    else:
                        protein += np.ceil(np.log(np.random.rand())/lp - 1)
                # Degrade protein
                else:
                    protein -= 1
                # Update the time to phase change
                tnext -= dt
                # Update the simulation time
                t += dt 
                # Update the cell age
                age += dt
            else:
                # We reach final time before next reaction occurs
                # Check if we need to save measurements first
                while Tmax > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    Tm += dTm
                # Final time reached, break to end simulation
                break
        # 2) Phase progression
        else:
            dt = tnext
            if t + dt < Tmax:               
                # Check if we need to save measurements
                while t + dt > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    Tm += dTm
                # Update the simulation time
                t += dt    
                # Generate the next phase change time
                # Check if we can use pre-generated random variables
                if rx < R:
                    tnext = Dt[rx]
                    rx += 1
                # Otherwise generate explicit one-by-one
                else:
                    tnext = - np.log(np.random.rand())/Ncycle
                # Update the phase
                if phase == Ncycle - 1:
                    # Apply the binomial partitioning
                    if protein > 0:
                        protein = np.random.binomial(protein, 0.5)
                    # Progress to first phase
                    phase = 0                            
                    burst_rate = b1  
                elif phase == Mcycle - 1:
                    # Progress to next phase
                    phase += 1
                    # Replication adds to burst rate
                    burst_rate = b2
                else:
                    # Progress to next phase
                    phase += 1
                # Update cell age                                
                age = 0                                
            else:
                # We reach final time before next reaction occurs
                # Check if we need to save measurements first
                while Tmax > Tm:
                    P[0,px] = Tm
                    P[1,px] = protein
                    P[2,px] = age + (Tm-t)
                    P[3,px] = phase
                    px += 1
                    Tm += dTm
                # Final time reached, break to end simulation
                break

    # Return the actual observations, i.e. up until last written index
    return P[:,:px]